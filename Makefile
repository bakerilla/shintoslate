PWD:=$(shell pwd)

generate-md:
	# widdershins --environment env.json swagger.json -o myOutput.md
	@widdershins petstore.yml -o index.html.md

build-static-site-dracula: generate-md
	docker run --rm --name slate -v ${PWD}/build/dracula:/srv/slate/build -v ${PWD}/templates/dracula:/srv/slate/source slatedocs/slate

build-static-site-default: generate-md
	docker run --rm --name slate -v ${PWD}/build/default:/srv/slate/build -v ${PWD}/index.html.md:/srv/slate/source/index.html.md slatedocs/slate

